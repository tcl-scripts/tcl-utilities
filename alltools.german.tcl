# This is specific to each language
# So I override with a hardcoded proc
proc ordnumber {string {f "m"}} {
	if {[isnumber $string]} then {
		return ${string}.
	}
}