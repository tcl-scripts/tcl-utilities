# This is specific to each language
# So I override with a hardcoded proc
proc ordnumber {string {f "m"}} {
	if {[isnumber $string]} then {
		switch $string {
			1 { if { $f == "f" } { return "1re" } else { return "1er" } }
			2 { if { $f == "f" } { return "2de" }  else { return "2nd" } }
			default { return ${string}e }
		}
	}
}