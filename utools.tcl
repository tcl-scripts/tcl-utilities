# utools.tcl 1.6
#
# Various utilities
#
# Actually, only string utilities
# 
# Author: CrazyCat <crazycat@c-p-f.org>
# https://www.eggdrop.fr
# irc.zeolia.net #eggdrop

# CHANGELOG
#
# Version 1.0 : initial release
#
# Version 1.1 :
#    - isvowel : returns 1 if letter is a vowel, else 0
#    - noaccent : removes accents from a string, thanks to MenzAgitat
#
# Version 1.2 : add internationalization
#    - number_to_number is overrided to use a language file
#    - ordnumber is overrided with a language specific proc
#
# Version 1.3 :
#    - ldiff : returns elements in list1 which are not in list2 (MenzAgitat version)
#
# Version 1.4:
#    - lintersect : returns elements in list1 which are in list2
#
# Version 1.5:
#   - randString : create a random string
#      usage : randString <min> <max>
#   - modified ldiff and lintersect
#
# Version 1.6: Math tools
#   - decround : makes round with choice of number of decimals (default: 2)
#   - deceil : makes ceil with choice of number of decimals (default: 2)
#   - decfloor : makes floor with choice of number of decimals (default: 2)


# METHODS
#
# ucfirst :	Make a string's first character uppercase
#	usage : ucfirst "your sentence"
#
# ucwords : Uppercase the first character of each word in a string
#	usage : ucwords "your sentence"
#	nota : others characters in the string are not modified
#
#
# uclwords : Uppercase the first character of each word in a string,
#  lowercase others
#	usage : uclwords "your sentence"
#

############################################################
# PATH CONFIGURATION
############################################################
set tool-path "scripts/tcl-utilities/"

proc ucfirst {sentence} {
	set sentence [join $sentence]
	set f [string toupper [string index $sentence 0]]
	set sentence [string replace $sentence 0 0 $f]
	return $sentence
}

proc ucwords {sentence} {
	list asent
	foreach word [split [join $sentence]] {
		lappend asent [::string::ucfirst $word]
	}
	set sentence [join $asent]
	return $sentence
}

proc uclwords {sentence} {
	set sentence [::string::ucwords [string tolower [join $sentence]]]
	return $sentence
}

proc noaccent {data} {
	return [::tcl::string::map -nocase {
	"à" "a" "â" "a" "ä" "a" "ã" "a" "å" "a" "á" "a" "à" "a" "å" "a"
	"é" "e" "è" "e" "ê" "e" "ë" "e"
	"î" "i" "ï" "i" "î" "i" "í" "i" "ì" "i"
	"ô" "o" "ö" "o" "õ" "o" "ø" "o" "ò" "o" "ó" "o"
	"ù" "u" "û" "u" "ü" "u" "ú" "u"
	"ý" "y" "ÿ" "y"
	"ç" "c" "ð" "d" "ñ" "n" "š" "s" "ž" "z"
	} $data]
}

set vowel {"a" "e" "i" "o" "u" "y"}
proc isvowel {letter} {
	if {[lsearch $::string::vowel [string tolower $letter]] > 0} {
		return 1
	} else {
		return 0
	}
}

proc ldiff {list1 list2 {option -exact}} {
   if {$option ne "-nocase"} { set option -exact }
   return [lmap x $list1 {expr {[lsearch $option $list2 $x] < 0 ? $x : [continue]}}]
}

proc lintersect {list1 list2 {option -exact}} {
   if {$option ne "-nocase"} { set option -exact }
   return [lmap x $list1 {expr {[lsearch $option $list2 $x] >= 0 ? $x : [continue]}}]
} 

proc lremove {list1 elem {option -exact}} {
	if {$option ne "-nocase"} { set option -exact }
	return [lreplace $list1 [lsearch $option $list1 $elem] [lsearch $option $list1 $elem]]
}

proc randString { {min 1} {max 8} } {
   set chars "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"
   set size [expr {int(rand()*($max-$min+1)+$min)}]
   set range [string length $chars]
   set txt ""
   for {set i 0} {$i < $size} {incr i} {
      set pos [expr {int(rand()*$range)}]
      append txt [string index $chars $pos]
   }
   return $txt
}


proc decround {number {dec 2}} {
	return [format "%.${dec}f" $number]
}
	
proc ceilfloor {number dec {mode "ceil"}} {
	set number [expr $number * pow(10, $dec)]
	set number [expr ${mode}($number)]
	return [expr $number / pow(10, $dec)]
}
	
proc decfloor {number {dec 2}} {
	putlog [::conv::ceilfloor $number $dec floor]
}
	
proc decceil {number {dec 2}} {
	putlog [::conv::ceilfloor $number $dec]
}

#
# internationalization
#
#set lang2local [list "english" "en" "danish" "da" "french" "fr" "finnish" "fi" "german" "ge" ]
# Not used actually, "char map list unbalanced"

if {![catch {package require msgcat}] && $::language != "" && [string tolower $::language] != "english"} {
	if {[file exists "${tool-path}lang.[string tolower $::language].tcl"]} {
		# Will use lang2local when corrected
		i18n "${tool-path}lang.[string tolower $::language].tcl" [::tcl::string::map {"english" "en" "danish" "da" "french" "fr" "finnish" "fi" "german" "ge"} [string tolower $::language]]
		# Redo number_to_number with i18n
		eval "proc number_to_number \{number\} \{ putlog \"in new\"; switch -exact -- \$number \{ 0 \{ return \[ucfirst \[::msgcat::mc zero\]\] \} 1 \{ return \[ucfirst \[::msgcat::mc one\]\] \} 2 \{ return \[ucfirst \[::msgcat::mc two\]\] \} 3 \{ return \[ucfirst \[::msgcat::mc three\]\] \} 4 \{ return \[ucfirst \[::msgcat::mc four\]\] \} 5 \{ return \[ucfirst \[::msgcat::mc five\]\] \} 6 \{ return \[ucfirst \[::msgcat::mc six\]\] \} 7 \{ return \[ucfirst \[::msgcat::mc seven\]\] \} 8 \{ return \[ucfirst \[::msgcat::mc eight\]\] \} 9 \{ return \[ucfirst \[::msgcat::mc nine\]\] \} 10 \{ return \[ucfirst \[::msgcat::mc ten\]\] \} 11 \{ return \[ucfirst \[::msgcat::mc eleven\]\] \} 12 \{ return \[ucfirst \[::msgcat::mc twelve\]\] \} 13 \{ return \[ucfirst \[::msgcat::mc thirteen\]\] \} 14 \{ return \[ucfirst \[::msgcat::mc fourteen\]\] \} 15 \{ return \[ucfirst \[::msgcat::mc fifteen\]\] \} default \{ return \$number \} \} \}"
	}
	if {[file exists "${tool-path}alltools.[string tolower $::language].tcl"]} {
		source ${tool-path}alltools.[string tolower $::language].tcl
	}
}

proc i18n {msgfile {lang {}} } {
	if {$lang == ""} {set lang [string range [::msgcat::mclocale] 0 1] }
	if { [catch {open $msgfile r} fmsg] } {
		putlog "Could not open $msgfile for reading\n$fmsg"
	} else {
		putlog "Loading $lang file from $msgfile"
		while {[gets $fmsg line] >= 0} {
			lappend ll $line
		}
		close $fmsg
		::msgcat::mcmset $lang [join $ll]
		unset ll
	}
}
