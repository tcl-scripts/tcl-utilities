# Small collection of utilities

* **ucfirst** :	Make a string's first character uppercase
* **ucwords** : Uppercase the first character of each word in a string
* **uclwords** : Uppercase the first character of each word in a string,

Internationalization of *number_to_number* and *ordnumber*

## Installation
Set the good relative path in [utools.tcl](https://gitlab.com/tcl-scripts/tcl-utilities/blob/master/utools.tcl#L41) and load utools.tcl in your eggdrop :
`source scripts/tcl-utilities/utools.tcl`

## Nota bene
**This can only work with the 1.9 version of eggdrop because it needs the exposure of `$language`**