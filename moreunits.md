# Conversion system - Units explanations

## Unit.json file
The file consists in two parts, called _bases_ and _units_
### _bases_ part
This section describes how you can translate from a system to another one. It always contains at least 2 entries (pairs), as _imp2iso_ and _iso2imp_ which define how to convert from **imp**erial system to **iso** system and from **iso** system to **imp**erial system.

For each of thes entries, you need to add (if not exists) a type of unity (as length, weight, speed) and the formula to translate to the other system.

`"iso2imp": { "length": "%d/0.9144", "weight": "%d/28.35", "temp": "32+(%d*9/5)", "speed": "%d*0.62137"}`
- **length**: the base of the **iso** length (meters) is converted in base of the **imp** system (yards) with doing _meters / 0.9144_
- **weight**: the base of the **iso** weight (grams) is converted in base of the **imp** system (ounces) with doing _grams / 28.35_
- **temp**erature: the base of the **iso** temperature (Celsius) is converted in base of the **imp** system (fahrenheit) with doing _32 + (9/5)*Celsius_
- **speed**: the base of the **iso** speed (km/h) is converted in base of the **imp** system (mph) with doing _kmh * 0.62137_
### _units_ part
An unit is first defined by its system (iso or imp), and then its symbol (as oz, km/h, K, ...).

Each unit contains 4 fields:
- **name**: the usual name of the unit (used only for display)
- **tobase**: the formula to convert the unit to its base. If the unit is the base, the formula is _%d*1_
- **frombase**: the formula to convert the base to the unit. If the unit is the base, the formula is _%d*1_
- **type**: the type of unity (related to *bases* part)
