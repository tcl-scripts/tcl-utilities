#######################################
# Units converter
#
### Usage
# !conv <value><from_unit> <to_unit>
#  <value> : the initial value
#  <from_unit> : the initial unit
#  <to_unit> : the unit you want
# Example: !conv 60mph km/h
#
# !ulist
#  gives the list of known units
#
### Adding units
# Read moreunits.md

namespace eval conv {

	variable lang "fr"
	variable version 0.1a
	variable author "CrazyCat <https://www.eggdrop.fr>"
	
	package require json
	package require msgcat

	variable units
	variable iso {}
	variable imp {}
	variable types
	variable mypath "[file dirname [file normalize [info script]]]/"
	
	bind pub - !conv ::conv::do
	bind pub - !ulist ::conv::list
	
	proc init {} {
		set fi [open ${::conv::mypath}/units.json r]
		set ::conv::units [::json::json2dict [read -nonewline $fi]]
		close $fi
		foreach sys [dict keys [dict get $::conv::units units]] {
			foreach u [dict keys [dict get $::conv::units units $sys]] {
				lappend ::conv::${sys} $u
				lappend ::conv::types([dict get $::conv::units units $sys $u type]) "[dict get $::conv::units units $sys $u name] ($u)"
			}
		}
	}
	
	proc list {nick uhost handle chan text} {
		foreach {type units} [array get ::conv::types] { putserv "PRIVMSG $chan :$type: [join $units ", "]"}
	}
	
	proc do {nick uhost handle chan text} {
		set src [join [lindex [split $text] 0]]
		set dest [join [lindex [split $text] 1]]
		regexp {([\d\.\,]+)([^ ]{1,5}²?)} $src -> val unit
		if {![info exists val] || ![info exists unit]} {
			putserv "PRIVMSG $chan :$src is not valid"
			return
		}
		regsub -all , $val . val
		if {[string first . $val]==-1} {
			set val $val.0
		}
		set srcsys [::conv::getsys $unit]
		set vbase [::conv::2base $val [dict get $::conv::units units $srcsys $unit tobase]]
		set ubase [dict get $::conv::units units $srcsys $unit frombase]
		if {$dest eq "" || $dest eq $ubase} {
			putserv "PRIVMSG $chan :$src gives $vbase [dict get $::conv::units units $srcsys $ubase name]"
		} elseif {![dict exists $::conv::units units $srcsys $dest]} {
			set dstsys [::conv::getsys $dest]
			if {$dstsys == 0 || ![dict exists $::conv::units units $dstsys $dest]} {
				putserv "PRIVMSG $chan :$dest is not in my database"
			}
			set tr [dict get $::conv::units bases ${srcsys}2${dstsys} [dict get $::conv::units units $srcsys $unit type]]
			set newbase [2base $vbase $tr]
			set result [2base $newbase [dict get $::conv::units units $dstsys $dest frombase]]
			putserv "PRIVMSG $chan :$val [dict get $::conv::units units $srcsys $unit name] gives [::conv::decround $result] [dict get $::conv::units units $dstsys $dest name] ($srcsys => $dstsys)"
		} else {
			putserv "PRIVMSG $chan :$val [dict get $::conv::units units $srcsys $unit name] gives [::conv::decround [2base $vbase [dict get $::conv::units units $srcsys $dest frombase]]] [dict get $::conv::units units $srcsys $dest name] ($srcsys)"
		}
	}
	
	proc getsys {unit} {
		if {[lsearch -nocase $::conv::iso $unit]==-1 && [lsearch -nocase $::conv::imp $unit]==-1} {
			return 0
		} elseif {[lsearch -nocase $::conv::imp $unit]!=-1} {
			return imp
		} else {
			return iso
		}
	}
	
	proc 2base {val ratio} {
		regsub -all %d $ratio $val ratio
		return [expr $ratio]
	}
	
	proc decround {number {dec 2}} {
		return [format "%.${dec}f" $number]
	}
	
}

::conv::init
putlog "Units converter V${::conv::version} by ${::conv::author} loaded"