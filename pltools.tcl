# PLTools
#
# Small utilities for eggdrop management

## NOTE ##
# All commands are accessible to global +m (master)
# or to local +n (owner)
# If the user is +n, the results are restricted
# to the channels he owns.

## COMMANDS ##
# .getattr <attribute>
#    Return the value of <attribute> for each channel
# .toggleflag <flag attribute> [channel]
#    Toggle the attribute (must be of type flag) on channel
#    or all channels if none targeted
# .mcset [#chan[,#chan]] <[+|-]attr> [value] [+|-attr [value]] [+|-attr [value]]
#    Mass change value of attributes. See examples below.
# .achannels [attr]
#    Return list of channels with attr enables

## EXAMPLES ##
# - As said before, if user is local +n, only the channels
#    he owned will be affected
# - Strings containing spaces must be encapsuled in {}
# - To reset (empty) an attribute, set {} as value.
# * .mcset need-op {} need-invite {putserv "PRIVMSG $::owner :Invite me!"} mytest {}
#    This will empty need-op and mytest (string or int) and set
#    putserv "PRIVMSG $::owner :Invite me!" in need-invite
# * .mcset #chan1,#chan2 +guard -bitch wmsg {"hello guys"}
#    This will turn guard on, bitch off and set
#    wmsg to "hello guys" on #chan1 and #chan2

## CHANGELOG ##
# 0.3 :
#   - added achannels global procedure
#   - renamed .toggle to .toggleflag (less generic, avoid conflicts)

namespace eval pltools {

   variable version "0.3"
   variable author "CrazyCat"
   
   # Return value of an attribute for channels
   proc getattr {handle idx text} {
      set attr [lindex $text 0]
      if {$attr eq ""} {
         putlog "Error: use .getattr attribute"
         return
      }
      if {[::pltools::getatype $attr] eq "unknow"} {
         putlog "Error: $attr is not a valid attribute"
         return
      }
      foreach c [gethandlechans $handle] {
         putlog "$attr for $c : [channel get $c $attr]"
      }
   }

   # Toggle flag attributes
   proc toggle {handle idx text} {
      lassign [split $text] attr chan
      if {[catch {set atype [chansettype $attr]}]} {
         putlog "Sorry but $attr is not a valid attribute"
         return
      }
      if {$atype ne "flag"} {
         putlog "Sorry, $attr is not a flag attribute"
         return
      }
      set lchan {}
         foreach c [::pltools::gethandlechans $handle $chan] {
         if {[channel get $c $attr]==1} {
            channel set $c -$attr
         } else {
            channel set $c +$attr
         }
         lappend lchan $c
      }
      putlog "$attr toggled on [join $lchan ", "]"
   }

   # Mass set attributes
   proc mcset {handle idx text} {
      set arg [::pltools::txt2list $text]
      set chans {}
      set attrname ""
      set attrvals ""
      for {set i 0} {$i<[llength $arg]} {incr i} {
         if {$i==0 && [string index [lindex $arg $i] 0] eq "#"} {
            set chans [::pltools::gethandlechans $handle [lindex $arg $i]]
            continue
         } elseif {$i==0 && [string index [lindex $arg $i] 0] ne "#"} {
            set chans [::pltools::gethandlechans $handle]
         }
         set atype [::pltools::getatype [lindex $arg $i]]
         if {$atype eq "unknow"} {
            putlog "Unknow attribute: [lindex $arg $i] - Continue to next one"
            continue
         } elseif {$atype == "flag"} {
            foreach c $chans {
               putlog "channel set $c [lindex $arg $i]"
               channel set $c [lindex $arg $i]
            }
         } else {
            foreach c $chans {
               putlog "channel set $c [lindex $arg $i] {*}[lindex $arg $i+1]"
               channel set $c [lindex $arg $i] {*}[lindex $arg $i+1]
            }
            incr i
         }
      }
   }

   # Internal usage
   # returns type of an attribute or unknow
   proc getatype {attr} {
      set f [string index $attr 0]
      if {$f=="+" || $f=="-"} {
         set tmpattr [string range $attr 1 end]
      } else {
         set tmpattr $attr
      }
      if {[catch {set atype [chansettype $tmpattr]}]} {
         return "unknow"
      } else {
         return $atype
      }
   }

   # Returns channels with a specific attribute
   proc dccachannels {handle idx text} {
      set achans {}
      set chans [::pltools::gethandlechans $handle]
      if {$text eq ""} {
         putlog $chans
         return
      } else {
         if {[::pltools::getatype $text] ne "flag"} {
            putlog "$text is not a valid channel flag"
            return
         }
         foreach c $chans {
            if {[channel get $c $text]} {
               lappend achans $c
            }
         }
         if {[llength $achans]==0} {
            putlog "No channel with $text flag enabled"
         } else {
            putlog "$text enabled on [join $achans ", "]"
         }
      }
   }
   
   # returns list of channels belonging to a handle
   proc gethandlechans {{handle *} {channels ""}} {
      if {$channels ne "" && [string index $channels 0] eq "#"} {
         set lchans [split $channels ","]
      } else {
         set lchans [channels]
      }
      if {$handle eq "*" || [matchattr $handle +m]} {
         foreach c $lchans {
            if {[validchan $c]} {
               lappend validchans $c
            }
         }
      } else {
         foreach c $lchans {
            if {[validchan $c] && [matchattr $handle |+n $c]} {
               lappend validchans $c
            }
         }
      }
      return $validchans
   }

   # internal usage
   # Short utilities to convert text to list
   proc txt2list {text} {
      set rlist {}
      set tmp {}
      foreach e [split $text " "] {
         if {[llength $tmp]==0 && [string index $e 0] ne "\{"} {
            lappend rlist $e
         } elseif {[string index $e end] ne "\}"} {
            lappend tmp $e
         } else {
            lappend tmp $e
            lappend rlist [join $tmp " "]
            set tmp {}
         }
      }
      return $rlist
   }

   bind dcc - getattr ::pltools::getattr
   bind dcc - toggleflag ::pltools::toggle
   bind dcc - mcset ::pltools::mcset
   bind dcc - achannels ::pltools::dccachannels

   putlog "PLTools v${::pltools::version} by ${::pltools::author} loaded"
}

# Global namespace procedure
# can be used in scripts
# [achannels] return [channels]
# [achannels [attr] return filtered list
proc achannels {{attr ""}} {
   set achans {}
   if {$attr eq ""} {
      set achans [channels]
   } else {
      if {[::pltools::getatype $attr] ne "flag"} {
         throw {TCL ATTRUNK "$attr is unknow or not flag attribute"} "$attr is unknow or not flag attribute"
         return
      }
      foreach c [channels] {
         if {[channel get $c $attr]} {
            lappend achans $c
         }
      }
   }
   return $achans
}